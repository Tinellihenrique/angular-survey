import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  surveyForm = new FormGroup({
    questions : new FormControl({
    reponse1: new FormControl(),
    reponse2: new FormControl()
  }),
  coordonnees: new FormGroup({
    nom: new FormControl(),
    email: new FormControl()
  })    
});

  constructor() { }

  ngOnInit(): void {
  }
  onSubmit() {
    console.log(this.surveyForm.value);
 
    return false;
  }
  
}
